using System;
using System.Collections;
using UnityEngine;

public class AnimSprite : FSprite
{
	protected string sprite_string;
	protected string _sprite_string_extension;
	protected string _lastSpriteString;
	protected Hashtable animations = new Hashtable();
	protected Sprite_Animation _currentAnimation;
	protected float startTime;
	protected int _direction = 1;
	
	public AnimSprite (string _sprite) : base(_sprite)
	{
		sprite_string = _sprite;
		_lastSpriteString = sprite_string;
		_sprite_string_extension = "";
		addAnimation("base", 0,0);
		play ("base");
	}
	
	public struct Sprite_Animation
	{
		public string animName;
		public int animLength;
		public int fps;
	}
	
	public Sprite_Animation currentAnimation
	{
		get { return _currentAnimation; }
	}
	
	public bool play(string animationName)
	{
		if(animationName == null)
		{
			return false;
		}
		
		if(animations.ContainsKey(animationName))
		{
			_currentAnimation = (RadialObject.Sprite_Animation)animations[animationName];
			startTime = Time.time;
			return true;
		}
		
		return false;
	}
	
	public bool addAnimation(string animName, int animLength=0, int fps = 20)
	{
		if(animName != null && !animations.ContainsKey(animName))
		{
			Sprite_Animation newAnim = new Sprite_Animation();
			newAnim.animName = animName;
			newAnim.animLength = animLength;
			newAnim.fps = fps;
			animations[animName] = newAnim;
			
			return true;
		}
		
		return false;
	}
	
	public bool addAnimation(Sprite_Animation newAnim)
	{
		if(newAnim.animName != null && newAnim.animName != currentAnimation.animName)
		{
			if(!animations.ContainsKey(newAnim.animName))
			{
				animations.Add(newAnim.animName, newAnim);
				return true;
			}
			else { return false; }
		} else { return false; }
	}
	
	public void removeAnimation(Sprite_Animation removeAnim)
	{
		if(removeAnim.animName != null && animations.ContainsKey(removeAnim.animName))
		{
			animations[removeAnim.animName] = null;
		}
	}
	
	public void removeAnimation(string animName)
	{
		if(animName != null && animations.ContainsKey(animName))
		{
			animations[animName] = null;
		}
	}
	
	public void update()
	{
		int currentFrame = 0;
		
		if(_currentAnimation.animName != "" && _currentAnimation.animName != "base") 
		{
			_sprite_string_extension = "_" + _currentAnimation.animName;
			
			if(_currentAnimation.animLength > 0)
			{
				string currentFrame_s;
				currentFrame = (int)(((Time.time - startTime) * _currentAnimation.fps) % _currentAnimation.animLength);
				currentFrame_s = currentFrame.ToString();
				while(currentFrame_s.Length < 3) { currentFrame_s = "0" + currentFrame_s; } 
				_sprite_string_extension += "_" + currentFrame_s;
			}
		} else
		{
			_sprite_string_extension = "";
		}
		
		if(this.direction < 0) _sprite_string_extension += "_reverse";
		
		if(sprite_string + _sprite_string_extension != _lastSpriteString)
		{
			_element = Futile.atlasManager.GetElementWithName(sprite_string + _sprite_string_extension);
			_lastSpriteString = sprite_string + _sprite_string_extension;
		}
		return;
	}
	
	public int direction
	{
		get { return _direction; }
		set { _direction = value; }
	}

}

