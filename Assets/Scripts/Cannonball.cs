using System;
using System.Collections;
using UnityEngine;

public class Cannonball : RadialObject 
{
	private int GRAVITY = 100;
	private const float ACTIVATION_TIME = .75f;
	private Ship.PlayerId playerID;
	private bool isActive;
	private float lifeTimer;

	public Cannonball(World _world, int _posTheta=0, int _posRad=100, float _deltaTheta = 10, float _deltaRad = 10, Ship.PlayerId _playerID = Ship.PlayerId.one) : base(_world, _posTheta, _posRad, true, "cannonball")
	{
		this.deltaRad = _deltaRad;
		this.deltaTheta = _deltaTheta;
		playerID = _playerID;
		isActive = false;
		lifeTimer = 0;
	}

	public Ship.PlayerId getPlayerID() {
		return playerID;
	}

	public new void update() 
	{
		if (isActive == false) {
			lifeTimer += Time.deltaTime;
			if (lifeTimer >= ACTIVATION_TIME)
			{
				isActive = true;
			}
		}
		
		float nextPosRad = posRad + (deltaRad * Time.deltaTime);
		
		if (worldCollision)
		{
			nextPosRad = worldCollisionResponse(nextPosRad);
		}

		this.deltaRad -= GRAVITY * Time.deltaTime;
		this.x -= (this.width / 2);
		this.y -= (this.height / 2);
		
		base.update();
	}

	public bool isShotActive()
	{
		return isActive;
	}

	protected new float worldCollisionResponse(float nextPosRad)
	{
		if (nextPosRad < World.WORLD_RADIUS)
		{
			this.kill();
		}

		return nextPosRad;
	}
}

