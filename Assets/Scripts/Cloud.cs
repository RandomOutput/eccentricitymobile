using System;
using System.Collections;
using UnityEngine;

public class Cloud : RadialObject
{
	private const int CLOUD_LAYER_HEIGHT = 140;
	private const int DIRECTION_CHANGE_TIME = 30;
	
	//private int windStrength; //force in: delta degrees per second 
	
	//How long has it been since the cloud last changed direction
	private float timeSinceLastDirectionChange;

	//How long has it been since the cloud last changed direction
	private float timeSinceLastStrengthChange;

	//private var flappingFlag:FlxSound;

	public Cloud(World _world, int _posTheta, int _windStrength) : base (_world, _posTheta, CLOUD_LAYER_HEIGHT, false, "cloud")
	{
		//windStrength = _windStrength;
		timeSinceLastDirectionChange = 0; 
		timeSinceLastStrengthChange = 0;
		
		/*
		flappingFlag = new FlxSound;
		flappingFlag.loadEmbedded(flappingFlagFile, false);
		flappingFlag.volume = .3;
		*/ 
	}

	private void updateStrength()
	{

	}

	private void updateDirection()
	{
		this.deltaTheta = -1 * deltaTheta;
	}

	public new void update()
	{
		timeSinceLastDirectionChange += Time.deltaTime;
		timeSinceLastStrengthChange += Time.deltaTime;

		if (timeSinceLastDirectionChange >= DIRECTION_CHANGE_TIME)
		{
			timeSinceLastDirectionChange = 0;
			updateDirection();
			//flappingFlag.play();
		}

		base.update();
	}

}


