﻿using UnityEngine;
using System.Collections;

public class EccentricityGame : MonoBehaviour {
	
	public enum State { Game, Menu };
	
	private FContainer currentState;
	
	// Use this for initialization
	void Start () {
		FutileParams fparams = new FutileParams(true, true, false, false);
		
		//fparams.AddResolutionLevel(480.0f,	1.0f,	1.0f, ""); //iPhone
		//fparams.AddResolutionLevel(960.0f,	2.0f,	2.0f, ""); //iPhone retina
		//fparams.AddResolutionLevel(1137.0f,	2.0f,	2.0f, ""); //iPhone 5
		fparams.AddResolutionLevel(2048.0f,	4.0f,	4.0f, "@2x"); //iPad Retina
		fparams.AddResolutionLevel(1024.0f,	2.0f,	2.0f, ""); //iPad
		
		fparams.origin = new Vector2(0.0f,0.0f);
		
		Futile.instance.Init(fparams);
		
		Futile.atlasManager.LoadAtlas("Atlases/Atlas1");
		Futile.atlasManager.LoadAtlas("Atlases/Atlas2");
		Futile.atlasManager.LoadAtlas("Atlases/fonts_001");
		//Futile.atlasManager.LoadFont("Franchise","FranchiseFont"+Futile.resourceSuffix, "Atlases/FranchiseFont"+Futile.resourceSuffix, 0.0f,-4.0f);
		Futile.atlasManager.LoadFont("Arial", "arial", "Atlases/arial", 0.0f, 0.0f);
		
		//PlayState gameState = new PlayState(this);
		//currentState = gameState;
		MenuState menuState = new MenuState(this);
		currentState = menuState;
		Futile.stage.AddChild(menuState);
	}
	
	public static Vector2 futileTouch(Vector2 touchPoint)
	{
		
		Vector2 result;
		float touchScale = 1.0f/Futile.displayScale;
		
		//the offsets account for the camera's 0,0 point (eg, center, bottom left, etc.)
		float offsetX = -Futile.screen.originX * Futile.screen.pixelWidth;
		float offsetY = -Futile.screen.originY * Futile.screen.pixelHeight;
		
		result = new Vector2((touchPoint.x+offsetX)*touchScale, (touchPoint.y+offsetY)*touchScale);
		
		return result;
	}
	
	public void changeState(State newState)
	{
		switch(newState)
		{
		case State.Game:
			Futile.stage.RemoveChild(currentState);
			currentState = null;
			
			PlayState gameState = new PlayState(this);
			currentState = (FContainer)gameState;
			Futile.stage.AddChild(gameState);
			break;
		case State.Menu:
			Futile.stage.RemoveChild(currentState);
			currentState = null;
			
			MenuState menuState = new MenuState(this);
			currentState = (FContainer)menuState;
			Futile.stage.AddChild(menuState);
			break;
		}
			
	}
}