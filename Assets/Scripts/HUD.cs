using System;
using System.Collections;
using UnityEngine;

public class Hud : FContainer
{
	private Vector2 heartOffset = new Vector2(8,29);
	private StatArray player1Hearts;
	private StatArray player1Shots;
	private StatArray player2Hearts;
	private StatArray player2Shots;
	
	private FSprite redCannon;
	private FSprite blueCannon;
	private FSprite redFlag;
	private FSprite blueFlag;
	
	private PlayState _parent;

	public Hud(PlayState parent) 
	{
		_parent = parent;
		
		redCannon = new FSprite("ui_cannon");
		redCannon.SetPosition((redCannon.width/2.0f) - 20, (redCannon.height/2.0f) - 20);
		blueCannon = new FSprite("ui_cannon");
		blueCannon.SetPosition(Futile.screen.width - (blueCannon.width/2.0f) + 20,Futile.screen.height - (redCannon.height/2.0f) + 20);
		blueCannon.rotation = 180;
		
		redFlag = new FSprite("red_sail");
		redFlag.SetPosition(Futile.screen.width - (redFlag.width/2.0f),redFlag.height/2.0f);
		blueFlag = new FSprite("blue_sail");
		blueFlag.SetPosition(redFlag.width / 2.0f,Futile.screen.height - (redFlag.height/2.0f));
		blueFlag.rotation = 180;
		
		Vector2[] p1Hearts = new Vector2[3];
		Vector2[] p1Shots = new Vector2[3];
		Vector2[] p2Hearts = new Vector2[3];
		Vector2[] p2Shots = new Vector2[3];
	
		p1Hearts[0] = new Vector2(0, 0);
		p1Hearts[1] = new Vector2(-10, 0);
		p1Hearts[2] = new Vector2(-20, 0);

		p1Shots[0] = (new Vector2(0, 0));
		p1Shots[1] = (new Vector2(30,0));
		p1Shots[2] = (new Vector2(60,0));

		p2Hearts[0] = (new Vector2(0, 0));
		p2Hearts[1] = (new Vector2(-10,0));
		p2Hearts[2] = (new Vector2(-20,0));

		p2Shots[0] = (new Vector2(0,0));
		p2Shots[1] = (new Vector2(30,0));
		p2Shots[2] = (new Vector2(60, 0));

		player1Hearts = new StatArray(0, 0, p1Hearts, "ui_heart", 3);
		player2Hearts = new StatArray(0, 0, p2Hearts, "ui_heart", 3);
		
		player1Shots = new StatArray((redCannon.width * 0.75f), 20, p1Shots, "ui_cannonball", 3);
		player2Shots = new StatArray(Futile.screen.width - (blueCannon.width * 0.75f), Futile.screen.height - 20, p2Shots, "ui_cannonball", 3);
		player2Shots.rot = 180;
		
		AddChild(redCannon);
		AddChild(blueCannon);
		AddChild(redFlag);
		AddChild(blueFlag);
		AddChild(player1Hearts);
		AddChild(player1Shots);
		AddChild(player2Hearts);
		AddChild(player2Shots);	
	}

	public void update()
	{
		Vector2 rotatedVector;
		player1Hearts.setStat(PlayState.p1Hearts);
		player1Shots.setStat(_parent.p1.shots);
		player2Hearts.setStat(PlayState.p2Hearts);
		player2Shots.setStat(_parent.p2.shots);
		
		rotatedVector = Quaternion.Euler(new Vector3(0,0, -1 * _parent.p1.rotation)) * new Vector2(heartOffset.x * _parent.p1.direction, heartOffset.y);
		player1Hearts.SetPosition(_parent.p1.GetPosition() + rotatedVector);
		player1Hearts.rot = _parent.p1.rotation;
		player1Hearts.reverse = _parent.p1.direction;
		
		rotatedVector = Quaternion.Euler(new Vector3(0,0, -1 * _parent.p2.rotation)) * new Vector2(heartOffset.x * _parent.p2.direction, heartOffset.y);
		player2Hearts.SetPosition(_parent.p2.GetPosition() + rotatedVector);
		player2Hearts.rot = _parent.p2.rotation;
		player2Hearts.reverse = _parent.p2.direction;
		
		player1Hearts.update();
		player1Shots.update();
		player2Hearts.update();
		player2Shots.update();
	}
	
	public Rect getRedCannonRect()
	{
		return redCannon.GetTextureRectRelativeToContainer();
	}
	
	public Rect getBlueCannonRect()
	{
		return blueCannon.GetTextureRectRelativeToContainer();
	}
	
	public Rect getRedSailRect()
	{
		return redFlag.GetTextureRectRelativeToContainer();
	}
	
	public Rect getBlueSailRect()
	{
		return blueFlag.GetTextureRectRelativeToContainer();
	}

	public void player1Death()
	{
		
	}

	public void player2Death()
	{
		
	}
}

