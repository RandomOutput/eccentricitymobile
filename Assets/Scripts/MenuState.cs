using System;
using System.Collections;
using UnityEngine;

public class MenuState : FContainer
{
	private FSprite background;
	private MenuWorld menuWorld;
	private FSprite startText;
	private EccentricityGame _parent;
	private Vector2 theTouch;
	private TouchPhase touchPhase;
	//private var menuAudio:FlxSound;

	public MenuState(EccentricityGame parent) 
	{
		_parent = parent;
		/*
		menuAudio = new FlxSound();

		menuAudio.loadEmbedded(menuAudioFile, true);
		menuAudio.volume = 1;
		menuAudio.play();
		*/
		//background = new FSprite("startScreenImage");
		//menuWorld = new MenuWorld((int)Futile.screen.halfWidth, -30);
		startText = new FSprite("startButton");
		startText.SetPosition(Futile.screen.halfWidth, Futile.screen.halfHeight);
		//startText.SetPosition(new Vector2(Futile.screen.halfWidth, 50));

		//AddChild(background);
		//AddChild(menuWorld);
		AddChild(startText);
		ListenForUpdate(handleUpdate);
	}

	public void handleUpdate()
	{
		//menuWorld.update();
		
		Vector2 theTouch = new Vector2(-99,-99);
		if(Input.touchCount > 0)
		{
			theTouch = EccentricityGame.futileTouch(Input.GetTouch(0).position);
			touchPhase = Input.GetTouch(0).phase;
		} else { touchPhase = TouchPhase.Canceled; }

		if (Input.GetKeyDown(KeyCode.G) || 
			Input.GetKeyDown(KeyCode.H) ||
			startText.GetTextureRectRelativeToContainer().Contains(theTouch) && touchPhase == TouchPhase.Ended)
		{
			//menuAudio.stop();
			//menuAudio.destroy();
			
			_parent.changeState(EccentricityGame.State.Game);
		}
	}
}

