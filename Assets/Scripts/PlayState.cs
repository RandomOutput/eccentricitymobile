using UnityEngine;
using System.Collections;
using System;

public class PlayState:FContainer
{
	private FLabel touchLoc;
	private FLabel ftouchLoc;
	private static uint ticks = 0;
	
	private EccentricityGame _parent;
	
	private const bool DEBUG_DISTANCE = false;
	private const bool DEBUG_SHOOTING = false;
	private const bool DEBUG_COLLISION = false;

	//Game Stats
	public static int p1Hearts = 3;		
	public static int p2Hearts = 3;		
	public static bool gameOver = false;

	private const int MAX_HEARTS = 3;
	private const int MAX_SHOTS = 3;
	private const int SHOT_RELOAD_SECONDS = 5;
	
	private const int BOID_COUNT = 8;
	private const int WIND_POWER = 6;
	private const int BASE_SPEED = 1;
	private const int CANNON_POWER = 15; //radial speed
	private const int CANNON_RAD_VELOCITY = 100;//height	
	private const float GAME_OVER_PAUSE_TIME = 3.5f;
	private const float WAVE_COUNT = 60.0f;
	private const float WAVE_SCALE = 1.125f;
	
	private Vector2 flagOffset = new Vector2(3,18);
	
	private RadialObject[] waves = new RadialObject[(int)WAVE_COUNT];


	//Background
	private FSprite background;

	//Game Objects
	private FSprite testSprite;
	private World world;
	private Ship player1;
	private Ship player2;
	private Cloud primeCloud;
	
	private AnimSprite p1Flag;
	private AnimSprite p2Flag;

	//Audio
	/*
	private var sailOpen:FlxSound;
	private var sailClose:FlxSound;
	private var backgroundCombined:FlxSound;
	private var fire:FlxSound;
	private var hit:FlxSound;
	*/

	//Groups
	private FContainer clouds;
	private FContainer ships;
	private FContainer shots;
	private FContainer boid_school;
	private Hud hud;
	
	private ArrayList touches = new ArrayList();
	
	private debugRect redShipDebug;
	private debugRect blueShipDebug;
	
	//Stores the touch points in game space
	private struct TouchPoint
	{
		public Vector2 touchPos;
		public TouchPhase phase;
	}
	
	public PlayState (EccentricityGame parent)
	{
		_parent = parent;
		
		ListenForUpdate(handleUpdate);
		
		//init game objects
		p1Hearts = 3;			
		p2Hearts = 3;		
		gameOver = false;

		world = new World((int)Futile.screen.halfWidth, (int)Futile.screen.halfHeight);
		player1 = new Ship(world, 180, World.WORLD_RADIUS+2, Ship.PlayerId.one);
		player2 = new Ship(world, 0, World.WORLD_RADIUS+2, Ship.PlayerId.two);
		
		primeCloud = new Cloud(world, 90, 20);
		
		p1Flag = new AnimSprite("flag");
		p1Flag.addAnimation("flap",6,10);
		p1Flag.SetAnchor(0.0f,0.5f);
		p1Flag.play("flap");
		
		p2Flag = new AnimSprite("flag");
		p2Flag.addAnimation("flap",6,10);
		p2Flag.SetAnchor(0.0f,0.5f);
		p2Flag.play("flap");

		//init groups
		ships = new FContainer();
		shots = new FContainer();
		clouds = new FContainer();
		boid_school = new FContainer();
		hud = new Hud(this);

		ships.AddChild(player1);
		ships.AddChild(player2);
		
		clouds.AddChild(primeCloud);

		//AddChild(background);
		
		AddChild(p1Flag);
		AddChild(p2Flag);
		AddChild(ships);
		
		for(int i=0;i<WAVE_COUNT/2;i++)
		{
			RadialObject newWave = new RadialObject(world, (int)((360.0f/(WAVE_COUNT/2)) * i), World.WORLD_RADIUS - 10, false, "waves/wave");
			newWave.deltaTheta = 5;
			newWave.addAnimation("000",0,0);
			newWave.addAnimation("001",0,0);
			newWave.addAnimation("002",0,0);
			newWave.addAnimation("003",0,0);
			newWave.addAnimation("004",0,0);
			newWave.addAnimation("005",0,0);
			newWave.addAnimation("006",0,0);
			newWave.addAnimation("007",0,0);
			newWave.addAnimation("008",0,0);
			newWave.addAnimation("009",0,0);
			newWave.addAnimation("010",0,0);
			newWave.addAnimation("011",0,0);
			AddChild(newWave);
			waves[i] = newWave;
			waves[i].startSeed = i * 20;
			waves[i].scale = WAVE_SCALE;
		}
		
		for(int i=(int)WAVE_COUNT/2;i<WAVE_COUNT;i++)
		{
			RadialObject newWave = new RadialObject(world, (int)((360.0f/(WAVE_COUNT/2)) * i), World.WORLD_RADIUS - 12, false, "waves/wave_alt");
			newWave.deltaTheta = -5;
			newWave.addAnimation("000",0,0);
			newWave.addAnimation("001",0,0);
			newWave.addAnimation("002",0,0);
			newWave.addAnimation("003",0,0);
			newWave.addAnimation("004",0,0);
			newWave.addAnimation("005",0,0);
			newWave.addAnimation("006",0,0);
			newWave.addAnimation("007",0,0);
			newWave.addAnimation("008",0,0);
			newWave.addAnimation("009",0,0);
			newWave.addAnimation("010",0,0);
			newWave.addAnimation("011",0,0);
			AddChild(newWave);
			waves[i] = newWave;
			waves[i].startSeed = i * 30;
			waves[i].scale = WAVE_SCALE;
		}
		
		AddChild(world);
		
		for(int i=0;i<BOID_COUNT;i++)
		{
			RadialBoid newBoid = new RadialBoid(world, UnityEngine.Random.Range(0,180), UnityEngine.Random.Range(0,World.WORLD_RADIUS-10),World.WORLD_RADIUS-10, "fish");
			newBoid.addAnimation("swim",4,10);
			newBoid.play("swim");
			newBoid.alpha = UnityEngine.Random.Range(0.4f,1.0f);
			boid_school.AddChild(newBoid);
		}
		
		AddChild(boid_school);
		
		AddChild(clouds);
		AddChild(shots);
		AddChild(hud);

		primeCloud.deltaTheta = 15;
		

		//super.create();
	}
	
	public static bool overlap(FSprite obj1, FSprite obj2)
	{
		Rect RectA = obj1.GetTextureRectRelativeToContainer();
		Rect RectB = obj2.GetTextureRectRelativeToContainer();

		if(RectA.xMin < RectB.xMax && RectA.xMax > RectB.xMin && RectA.yMin < RectB.yMax && RectA.yMax > RectB.yMin)  return true;
		
		return false;
	}
	
	public static bool overlap(Rect RectA, Rect RectB)
	{
		if(RectA.xMin < RectB.xMax && RectA.xMax > RectB.xMin && RectA.yMin < RectB.yMax && RectA.yMax > RectB.yMin)  return true;
		
		return false;
	}
	
	public Ship p1
	{
		get { return player1; }
	}
	
	public Ship p2
	{
		get { return player2; }
	}
	
	public void handleUpdate()
	{	
		Cloud cloud;
		float cloudLoc;
		float distance;
		int frame;
		Vector2 rotatedVector;
		
		touches.Clear();
		world.update();
		hud.update();
		ticks++;
		
		//Get Touches
		for(int i=0; i<Input.touchCount; i++)
		{
			TouchPoint newTouch = new TouchPoint();
			newTouch.touchPos = EccentricityGame.futileTouch(Input.GetTouch(i).position);
			newTouch.phase = Input.GetTouch(i).phase;
			touches.Add(newTouch);
		}
		
		for(int i=0;i<BOID_COUNT;i++)
		{
			RadialBoid boid = (RadialBoid)boid_school.GetChildAt(i);
			boid.update(boid_school);
		}
		
		//WAVE FRAMES
		for(int i=0;i<WAVE_COUNT;i++)
		{
			cloud = (Cloud)clouds.GetChildAt(0);	
			float dist = getRotationalDistance(waves[i].getPosTheta(), cloud.getPosTheta());
			
			if(dist < 0) waves[i].direction = -1; else waves[i].direction = 1;
			
			float distFinal = Math.Abs(dist) / 180.0f;
			distFinal = (distFinal * 11.0f);
			distFinal = (float)Math.Round(distFinal);
			
			frame = (int)Math.Max(Math.Round(((Math.Sin((Time.time*6) + ((180-Math.Abs(dist))/9))+1)*0.5) * 11), distFinal);
			
			if(frame < 10) waves[i].play("00"+frame);
			else waves[i].play ("0" + frame);
			waves[i].update();
		}
		
		if (!gameOver)
		{
			for(int i=0;i<clouds.GetChildCount();i++)
			{
				cloud = (Cloud)clouds.GetChildAt(i);
				cloud.update();
			}
			
			for(int i=0;i<shots.GetChildCount();i++)
			{
				Cannonball shot = (Cannonball)shots.GetChildAt(i);
				shot.update();
			}
			
			for(int i=0;i<ships.GetChildCount();i++)
			{
				Ship player = (Ship)ships.GetChildAt(i);
				float playerLoc = player.getPosTheta();
				
				if (Input.GetKey(KeyCode.Q)) {
					_parent.changeState(EccentricityGame.State.Menu);
				}

				//*** RESPAWN SHOTS ***
				if (player.shotRespawnTimer >= SHOT_RELOAD_SECONDS && player.shots < MAX_SHOTS)
				{
					player.shotRespawnTimer = 0;
					player.shots++;
				}
				else if (player.shotRespawnTimer < SHOT_RELOAD_SECONDS && player.shots < MAX_SHOTS)
				{
					player.shotRespawnTimer += Time.deltaTime;
				}

				//*** CHECK FOR COLLISIONS ***
				for(int j=0;j<shots.GetChildCount();j++)
				{
					Cannonball shot  = (Cannonball)shots.GetChildAt(j);
					
					if (shot.isShotActive() && overlap(player, shot)) 
					{
						if (player.playerID == Ship.PlayerId.one)
						{
							p1Hearts -= 1;
							if (p1Hearts <= 0) {
								hud.player1Death();
								gameOver = true;
								player1.kill();
							}
						}
						else 
						{
							p2Hearts -= 1;
							if (p2Hearts <= 0) {
								hud.player2Death();
								gameOver = true;
								player2.kill();
							}
						}
						shot.kill();
					}
				}
				
				for(int k=0;k<clouds.GetChildCount();k++)
				{
					cloud = (Cloud)clouds.GetChildAt(k);
					cloudLoc = cloud.getPosTheta();
					distance = getRotationalDistance(playerLoc, cloudLoc);
					
					if(distance < 0)
					{
						if(player.playerID == Ship.PlayerId.one)
						{
							p1Flag.direction=-1;
							p1Flag.SetAnchor(1.0f,0.5f);
						} else 
						{
							p2Flag.direction=-1;
							p2Flag.SetAnchor(1.0f,0.5f);
						}
					}
					else
					{
						if(player.playerID == Ship.PlayerId.one)
						{
							p1Flag.direction=1;
							p1Flag.SetAnchor(0.0f,0.5f);
						} else 
						{
							p2Flag.direction=1;
							p2Flag.SetAnchor(0.0f,0.5f);
						}
					}
					
					//If player has opened their ship's sail
					if ((Input.GetKey(KeyCode.G) && player.playerID == Ship.PlayerId.one) || 
						(Input.GetKey(KeyCode.H) && player.playerID == Ship.PlayerId.two) ||
						(checkAgainstTouches(hud.getRedSailRect()) && player.playerID == Ship.PlayerId.one) ||
						(checkAgainstTouches(hud.getBlueSailRect()) && player.playerID == Ship.PlayerId.two)) 
					{
						player.play("base");
						
						if (distance > 0) { player.deltaTheta += ((180 - Math.Abs(distance)) / (1000 - WIND_POWER)) + BASE_SPEED; }
						else { player.deltaTheta -= ((180 - Math.Abs(distance)) / (1000 - WIND_POWER)) + BASE_SPEED; }
					}
					else { player.play("down"); }
				}

				//If player is firing a bomb
				if ((Input.GetKeyDown(KeyCode.T) && player.playerID == Ship.PlayerId.one && p1.shots > 0) ||
					(Input.GetKeyDown(KeyCode.Y) && player.playerID == Ship.PlayerId.two && p2.shots > 0) ||
					(checkAgainstTouches(hud.getRedCannonRect(), TouchPhase.Ended) && player.playerID == Ship.PlayerId.one && p1.shots > 0) ||
					(checkAgainstTouches(hud.getBlueCannonRect(), TouchPhase.Ended) && player.playerID == Ship.PlayerId.two && p2.shots > 0)) 
				{	
					spawnCannonball(player.deltaTheta, player.getCannonLocation(), player.playerID);

					player.shots -= 1;
				}
				
				player.update();
			}
			
			rotatedVector = Quaternion.Euler(new Vector3(0,0, -1 * p1.rotation)) * new Vector2(flagOffset.x * p1.direction, flagOffset.y);
			p1Flag.SetPosition(p1.GetPosition() + rotatedVector);
			p1Flag.rotation = p1.rotation;
			p1Flag.update(); 
			
			rotatedVector = Quaternion.Euler(new Vector3(0,0, -1 * p2.rotation)) * new Vector2(flagOffset.x * p2.direction, flagOffset.y);
			p2Flag.SetPosition(p2.GetPosition() + rotatedVector);
			p2Flag.rotation = p2.rotation;
			p2Flag.update();
			
			ArrayList shotsToRemove = new ArrayList();
			for(int i=0;i<shots.GetChildCount();i++)
			{
				Cannonball shot = (Cannonball)shots.GetChildAt(i);
				if(shot.state == RadialObject.ObjectState.dead) { shotsToRemove.Add(shots.GetChildAt(i)); }
			}
			
			foreach(Cannonball shot in shotsToRemove) { shots.RemoveChild(shot); }
		}
		else //if game over
		{
			_parent.changeState(EccentricityGame.State.Menu);
		}
	}
	
	private float getRotationalDistance(float obj1Loc, float obj2Loc)
	{
		int obj1Sign = 1;
		int obj2Sign = 1;

		if (obj1Loc < 0) {
			obj1Loc = Math.Abs(obj1Loc);
			obj1Sign = -1;
		}

		if (obj2Loc < 0) {
			obj2Loc = Math.Abs(obj2Loc);
			obj2Sign = -1;
		}

		if (Math.Abs(obj1Loc) > 360)
		{
			if (obj1Loc > 0) { obj1Loc = obj1Loc % 360; }
			else { obj1Loc = -1 * (Math.Abs(obj1Loc) % 360); }
		}

		if (Math.Abs(obj2Loc) > 360)
		{
			if (obj2Loc > 0) { obj2Loc = obj2Loc % 360; }
			else { obj2Loc = -1 * (Math.Abs(obj2Loc) % 360); }
		}

		if (obj1Sign < 0) { obj1Loc = 360 - obj1Loc; }
		if (obj2Sign < 0) { obj2Loc = 360 - obj2Loc; }

		//get angle
		return angleDifference(obj2Loc, obj1Loc);
	}
	
	private bool checkAgainstTouches(Rect touchZone, TouchPhase goalPhase = TouchPhase.Stationary)
	{
		foreach(TouchPoint touchPoint in touches)
		{
			if(touchZone.Contains(touchPoint.touchPos))
			{
				if(goalPhase != TouchPhase.Stationary && goalPhase != touchPoint.phase)//if the phase has been set, and the phase isn't the same.
				{
					return false;
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	//via http://www.codeproject.com/Articles/59789/Calculate-the-real-difference-between-two-angles-k
	private float angleDifference(float angle0, float angle1)
	{
		float diff = angle1 - angle0;
		while (diff < -180) diff += 360;
		while (diff > 180) diff -= 360;
		return diff;

	}

	public void spawnCannonball(float spawnVelocity, Vector2 spawnLocation, Ship.PlayerId playerID)
	{
		float spawnTheta;
		float spawnRad;
		float spawnDeltaTheta;

		Vector2 direction = spawnLocation - world.GetPosition();
		spawnTheta = 360 - (Mathf.Rad2Deg * (float)Math.Atan2((float)(direction.y), (float)(direction.x)));
		spawnRad = Math.Abs(Vector2.Distance(world.GetPosition(), spawnLocation));

		if (spawnVelocity >= 0)
		{
			spawnDeltaTheta = spawnVelocity + CANNON_POWER;
		}
		else 
		{
			spawnDeltaTheta = spawnVelocity - CANNON_POWER;
		}

		Cannonball newShot = new Cannonball(world, (int)spawnTheta, (int)spawnRad, (int)spawnDeltaTheta, CANNON_RAD_VELOCITY, playerID);
		shots.AddChild(newShot);

		return;
	}
	/*
	public void kill()
	{
		//backgroundCombined.stop();
		//backgroundCombined.destroy();
		//super.kill();
	}
	*/
	
}

