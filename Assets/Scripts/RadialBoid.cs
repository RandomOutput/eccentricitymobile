using System;
using System.Collections;
using UnityEngine;

public class RadialBoid : RadialObject
{
	private const float MAX_SPEED = 30.0f;
	private FContainer _otherBoids;
	private int _maxRad;
	private Vector2 lastPos;
	
	public RadialBoid (World _world, int _posTheta, int _posRad, int maxRad, string _sprite) : base(_world, _posTheta, _posRad, false, _sprite)
	{
		_maxRad = maxRad;
	}
	
	public void update(FContainer boids)
	{
		_otherBoids = boids;
		Vector2 v1 = pullIn();
		Vector2 v2 = pushOut();
		Vector2 v3 = shareDirection();
		Vector2 v4 = clampAltitude();
		
		this.deltaTheta += (v1.x + v2.x + v3.x + v4.x);
		this.deltaRad += (v1.y + v2.y + v3.y + v4.y);
		
		this.deltaTheta = clampSpeed().x;
		this.deltaRad = clampSpeed().y;
		
		lastPos = this.GetPosition();
		base.update();
		Vector2 diff = this.GetPosition() - lastPos;
		this.rotation = (Mathf.Rad2Deg * (float)Math.Atan2(-1 * diff.y, diff.x));
	}
	
	public Vector2 clampSpeed()
	{
		float speed = (new Vector2(this.deltaTheta, this.deltaRad)).magnitude;
		
		if(speed > MAX_SPEED)
		{
			return (new Vector2(this.deltaTheta, this.deltaRad) / speed) * MAX_SPEED;
		} else
		{
			return new Vector2(this.deltaTheta, this.deltaRad);
		}
	}
	
	public Vector2 pullIn()
	{
		Vector2 resultVector = new Vector2(0,0);
		
		for(int i=0;i<_otherBoids.GetChildCount();i++)
		{
			RadialBoid boid2 = (RadialBoid)_otherBoids.GetChildAt(i);
			
			if(this != boid2)
			{
				resultVector += new Vector2(boid2.posTheta, boid2.posRad);
			}
		}
		
		resultVector = resultVector / (float)(_otherBoids.GetChildCount()-1.0f);
		return ((resultVector - new Vector2(this.posTheta, this.posRad)) / 100.0f);
	}
	
	public Vector2 pushOut()
	{
		Vector2 resultVector = new Vector2(0,0);
		
		for(int i=0;i<_otherBoids.GetChildCount();i++)
		{
			RadialBoid boid2 = (RadialBoid)_otherBoids.GetChildAt(i);
			
			if(boid2 != this)
			{
				if(Math.Abs((new Vector2(boid2.posTheta, boid2.posRad) - new Vector2(this.posTheta, this.posRad)).magnitude) < 10)
				{
					resultVector = resultVector - (new Vector2(boid2.posTheta, boid2.posRad) - new Vector2(this.posTheta, this.posRad));
				}
			}
		}
		
		return resultVector;
	}
	
	public Vector2 shareDirection()
	{
		Vector2 resultVector = new Vector2(0,0);
		
		for(int i=0;i<_otherBoids.GetChildCount();i++)
		{
			RadialBoid boid2 = (RadialBoid)_otherBoids.GetChildAt(i);
			
			if(boid2 != this)
			{
				resultVector = resultVector + new Vector2(boid2.deltaTheta, boid2.deltaRad);
			}
		}
		
		resultVector = resultVector / (float)(_otherBoids.GetChildCount() - 1);
		
		return (resultVector - new Vector2(this.deltaTheta, this.deltaRad))/100.0f;
	}
	
	public Vector2 clampAltitude()
	{
		
		//Debug.Log("posRad: " + (this.posRad));
		if(this.posRad > _maxRad) 
		{
			//Debug.Log ("------distance: " + (_maxRad-this.posRad));
			return new Vector2(0,-2);
		}
		else if(this.posRad < 40)
		{
			return new Vector2(0,2);
		} else
		{
			return new Vector2(0,0);
		}
	}
}

