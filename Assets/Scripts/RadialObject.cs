using System;
using UnityEngine;
using System.Collections;


public class RadialObject : AnimSprite 
{
	private const bool LOCATION_DEBUG = false;
	private const int FPS = 10;
	public enum ObjectState { 
		alive,
		dead
	};

	public float posTheta;
	public float posRad;
	public float deltaTheta; //degrees per second
	public float deltaRad; //pixels per second
	public FSprite selfSprite;
	protected World world;
	protected bool worldCollision;
	public ObjectState state = ObjectState.alive;
	public float startSeed;
	

	public RadialObject(World _world, int _posTheta = 0, int _posRad = 100, bool _worldCollision = false, string _sprite="red_ship") : base(_sprite)
	{
		posTheta = _posTheta;
		posRad = _posRad;
		world = _world;
		worldCollision = _worldCollision;
		deltaRad = 0;
		deltaTheta = 0;
		startSeed = UnityEngine.Random.Range(0,1000);
		
	}
	
	
	
	public new void update() 
	{
		//convert radius to base origin
		float nextPosTheta = posTheta + (deltaTheta * Time.deltaTime);
		float nextPosRad = posRad + (deltaRad * Time.deltaTime);

		if (worldCollision)
		{
			nextPosRad = worldCollisionResponse(nextPosRad);
		}

		posTheta = nextPosTheta;
		posRad = nextPosRad;

		setXYPos();
		setRotation();
		base.update();
	}

	protected float worldCollisionResponse(float nextPosRad)
	{
		if (nextPosRad < World.WORLD_RADIUS)
		{
			nextPosRad = World.WORLD_RADIUS + this.height;
		}

		return nextPosRad;
	}

	public float getCenterTheta()
	{
		Vector2 centerPoint = new Vector2(this.x + (this.width / 2), this.y + (this.height / 2));
		return Vector2.Angle(world.getCenterPoint(), centerPoint);
		//TODO: Fix
		//return FlxU.getAngle(world.getCenterPoint(), centerPoint);
	}

	protected void setXYPos()
	{
		//x=r * cos(theta)
		//y=r * sin(theta)
		this.x = world.x + ((posRad + (this.height/2.0f)) * (float)Math.Cos(getRadians(posTheta)));
		this.y = world.y - ((posRad + (this.height / 2.0f)) * (float)Math.Sin(getRadians(posTheta)));
	}

	protected Vector2 getCenterPoint(bool objectSpace = false)
	{
		if (objectSpace)
			return new Vector2((this.width/2),(this.height/2));
		else
			return this.GetPosition();
	}

	public float getPosTheta(bool inRadians = false)
	{
		if (inRadians == true) 
		{
			return getRadians(posTheta);
		}
		else
		{
			return posTheta;
		}
	}
	
	protected float getRadians(float degrees)
	{
		//radians = degrees*Math.PI/180
		return degrees * (float)(Math.PI / 180.0f);
	}

	protected void setRotation()
	{
		//float angle = Vector2.Angle(world.getCenterPoint(), new Vector2(this.x, this.y));
		Vector2 direction = world.GetPosition() - new Vector2(this.x, this.y);
		float angle = 360 - (Mathf.Rad2Deg * (float)Math.Atan2((float)(direction.y), (float)(direction.x)));
		this.rotation = angle-90;
	}
	
	public void kill()
	{
		this.state = ObjectState.dead;
	}

}

