using System;
using System.Collections;
using UnityEngine;

public class Ship : RadialObject
{
	public enum PlayerId { one, two };
	private static Vector2 CANNON_LOCATION = new Vector2(-9, 10);
	private const int MAX_SPEED = 45;
	private float friction = .996f;
	public float shotRespawnTimer = 0;
	private AnimSprite flag;
	public int shots = 0;

	public PlayerId playerID;

	//public int shotsLeft;

	public Ship(World _world, int _posTheta = 0, int _posRad = 100, PlayerId _playerID = PlayerId.one) : base(_world, _posTheta, _posRad, true)
	{
		playerID = _playerID;
		FAtlasManager am = Futile.atlasManager;
		if (playerID == PlayerId.one)
		{	
			sprite_string = "red_ship";
			/*
			addAnimation("Sail Open Right", [0], 0, false);
			addAnimation("Sail Closed Right", [1], 0, false);
			addAnimation("Sail Open Left", [4], 0, false);
			addAnimation("Sail Closed Left", [5], 0, false);
			play("Sail Closed", true);
			*/
		}
		else
		{
			sprite_string = "blue_ship";
			/*
			addAnimation("Sail Open Right", [2], 0, false);
			addAnimation("Sail Closed Right", [3], 0, false);
			addAnimation("Sail Open Left", [6], 0, false);
			addAnimation("Sail Closed Left", [7], 0, false);
			play("Sail Closed", true);
			*/
		}
		
		addAnimation("reverse",0,0);
		addAnimation("down",0,0);
		this.element = am.GetElementWithName(sprite_string);
	}
	
	

	public Vector2 getCannonLocation()
	{
		int cannonDirection = 1;
		if(deltaTheta < 0) cannonDirection = -1;
		Vector2 rotatedVector = Quaternion.Euler(new Vector3(0,0,-1 * rotation)) * new Vector2(CANNON_LOCATION.x * cannonDirection, CANNON_LOCATION.y);
		return this.GetPosition() + rotatedVector;
	}

	public new void update()
	{
		deltaTheta *= friction;

		if (deltaTheta > MAX_SPEED) {
			deltaTheta = MAX_SPEED;
		}
		
		if(deltaTheta < (-1 * MAX_SPEED)){
			deltaTheta = (-1 * MAX_SPEED);
		}
		
		if(deltaTheta < 0)
		{
			direction = -1;
		}
		else 
		{
			direction = 1;
		}

		base.update();
		
		this.rotation += (float)Math.Sin((Time.time*1.5) + startSeed) * 5;
	}
	/*
	public void draw() 
	{
		//var debugSprite:FlxSprite = new FlxSprite();
		//super.draw();
		//debugSprite.makeGraphic(2, 2, 0xFFFFFFFF);
		//debugSprite.x  = this.x;
		//debugSprite.y = this.y;
		//debugSprite.draw();
	}*/
}

