using System;
using System.Collections;
using UnityEngine;

public class StatArray:FContainer
{
	private FSprite[] icons;
	private Vector2[] locations;
	private int currentStat;
	public float rot;
	public int reverse = 1;
	private bool inverted = false;

	//objects takes location in OBJECT SPACE not WORLD SPACE
	public StatArray(float _x, float _y, Vector2[] _locations, string sprite, int statingStat, bool _inverted = false) 
	{
		this.x = _x;
		this.y = _y;
		icons = new FSprite[3];
		locations = _locations;
		currentStat = statingStat;
		inverted = _inverted;
		
		for(int i=0;i < 3; i++)
		{
			Vector2 location = locations[i];
			icons[i] = new FSprite(sprite);
			icons[i].SetPosition(new Vector2(location.x, location.y));
			if(inverted==true)icons[i].rotation=180;
			AddChild(icons[i]);
		}
	}

	public void update() 
	{
		Quaternion rotQuat = Quaternion.Euler(0,0,-1 * rot);
		
		for (int i = 0; i < 3; i++ )
		{
			if (i < currentStat)
				icons[i].isVisible = true;
			else	
				icons[i].isVisible = false;
			
			icons[i].SetPosition(rotQuat * new Vector2(locations[i].x * reverse, locations[i].y));
			icons[i].rotation = rot;
			
			if(inverted==true)icons[i].rotation+=180;
		}
	}

	public void draw()
	{
		//super.draw();
		/*
		foreach(FSprite icon in icons)
		{
			icon.draw();
		}*/
	}

	public bool incrementStat(int amount = 1)
	{
		if (currentStat + amount <= 3)
		{
			currentStat += amount;
			return true;
		}
		else
		{	
			currentStat = 3;
			return false;
		}
	}

	public bool decrementStat(int amount = 1)
	{
		if (currentStat - amount >= 0)
		{
			currentStat += amount;
			return true;
		}
		else
		{	
			currentStat = 0;
			return false;
		}
	}

	public bool setStat(int stat)
	{
		if (stat < 0)
		{
			currentStat = 0;
			return false;
		}
		else if(stat > 3)
		{	
			currentStat = 3;
			return false;
		}
		else 
		{
			currentStat = stat;
			return true;
		}
	}
}

