using System;
using System.Collections;
using UnityEngine;

public class World : FContainer
{
	public const int WORLD_RADIUS = 87;
	private const int OUTER_SPEED = 10;
	private const int INNER_SPEED = -7;
	
	private FSprite outerCircle;
	private FSprite innerCircle;
	private FSprite rock;
	
	private Vector2 pos;

	public World(int _x, int _y) 
	{
		this.x = _x;
		this.y = _y;

		//outerCircle = new FSprite("outer_circle");
		innerCircle = new FSprite("inner_circle");
		//Debug.Log("Anchor: " + outerCircle.GetAnchor());
		//outerCircle.SetAnchor(0.0f,1.0f);
		
		rock = new FSprite("theRock");
	

		innerCircle.x = 0;// + (outerCircle.textureRect.width / 2) - (innerCircle.textureRect.width / 2);
		innerCircle.y = 0;// + (outerCircle.textureRect.height / 2) - (innerCircle.textureRect.height / 2);

		rock.x = 0;// + (outerCircle.textureRect.width / 2) - (rock.textureRect.width / 2);
		rock.y = 0;// + (outerCircle.textureRect.height / 2) - (rock.textureRect.height / 2);
		
		//AddChild(outerCircle);
		AddChild(innerCircle);
		
		AddChild(rock);
	}

	public Vector2 getCenterPoint()
	{
		return this.GetPosition();
		//return new Vector2((this.x + outerCircle.textureRect.width/2), (this.y + outerCircle.textureRect.height / 2));
	}

	public void update()
	{
		//outerCircle.rotation += (OUTER_SPEED * Time.deltaTime);
		innerCircle.rotation += (INNER_SPEED * Time.deltaTime);
	}
	
	/*
	override public function draw():void
	{
		super.draw();
			innerCircle.draw();
			rock.draw();
		}
	}
	*/
}

