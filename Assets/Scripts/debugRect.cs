using UnityEngine;
using System;

public class debugRect : FSprite
{
	public float rect_width;
	public float rect_height;
	
	public debugRect (float _width, float _height) : base()
	{
		rect_width = _width;
		rect_height = _height;
		
		Init (FFacetType.Quad, Futile.atlasManager.GetElementWithName("Futile_White"), 1);
		this.alpha = 0.5f;
		
		_isAlphaDirty = true;
		UpdateLocalVertices();
	}
	
	override public void PopulateRenderLayer()
	{
		if(_isOnStage && _firstFacetIndex != -1)
		{
			Debug.Log("rendering them things");
			_isMeshDirty = false;
			int vertexIndex0 = _firstFacetIndex * 4;
			Vector3[] quadVerts = _renderLayer.vertices;
			Vector2[] quadUVVerts = _renderLayer.uvs;
			
			Vector2[] trueQuadVerts = new Vector2[4];
			Vector2[] textureUVVerts = new Vector2[4];
			
			Color[] colors = _renderLayer.colors;
			
			trueQuadVerts[0] = new Vector2(0,0);
			trueQuadVerts[1] = new Vector2(0,50);
			trueQuadVerts[2] = new Vector2(50, 50);
			trueQuadVerts[3] = new Vector2(50,0);
			
			textureUVVerts[0] = new Vector2(_element.uvBottomLeft.x, _element.uvBottomLeft.y);
			textureUVVerts[1] = new Vector2(_element.uvTopLeft.x,_element.uvTopLeft.y);
			textureUVVerts[2] = new Vector2(_element.uvTopRight.x, _element.uvTopRight.y);
			textureUVVerts[3] = new Vector2(_element.uvBottomRight.x, _element.uvBottomRight.y);
			
			for(int i=0; i<4;i++)
			{
				colors[vertexIndex0 + i] = _alphaColor;
			}
			
			/*
			for (int i = 0; i < 4; i++) {
				trueQuadVerts[i].x -= (rect_width / 2f);
				trueQuadVerts[i].y -= (rect_height / 2f);
			}*/
			
			_concatenatedMatrix.ApplyVector3FromLocalVector2(ref quadVerts[vertexIndex0 + 0], trueQuadVerts[0],0);
			_concatenatedMatrix.ApplyVector3FromLocalVector2(ref quadVerts[vertexIndex0 + 1], trueQuadVerts[1],0);
			_concatenatedMatrix.ApplyVector3FromLocalVector2(ref quadVerts[vertexIndex0 + 2], trueQuadVerts[2],0);
			_concatenatedMatrix.ApplyVector3FromLocalVector2(ref quadVerts[vertexIndex0 + 3], trueQuadVerts[3],0);
			
			quadUVVerts[vertexIndex0 + 0] = textureUVVerts[0];
			quadUVVerts[vertexIndex0 + 1] = textureUVVerts[1];
			quadUVVerts[vertexIndex0 + 2] = textureUVVerts[2];
			quadUVVerts[vertexIndex0 + 3] = textureUVVerts[3];
			
			_renderLayer.HandleVertsChange();
		}
	}
}

